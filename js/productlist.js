const ProductList = function () {
  this.arr = [];

  this.addProd = function (prod) {
    /**
     * Hàm push Thêm 1 phần tử vào mảng
     */
    this.arr.push(prod);
  };

  this.findIndexProd = function (id) {
    var index = -1;

    /**
     * Hàm forEach nhận tham số là 1 callback function (P/s: tham số của hàm lại là 1 hàm khác)
     * Trong callback function có 2 tham số
     *    - tham số thứ 1: đại diện cho từng phần tử khi duyệt qua mảng
     *    - tham số thứ 2: đại diện cho số chỉ mục của từng phần tử trong mảng
     * => Khi dùng forEach: không cần phải quan tạm mảng có độ dài bao nhiêu,
     *    duyệt đến khi nào hết mảng thì dừng
     */
    //Cach 1:
    // this.arr.forEach(function(item, i) {
    //   if (parseInt(item.id) === id) {
    //     index = i;
    //   }
    // });

    index = this.arr.findIndex(function (item) {
      return parseInt(item.id) === parseInt(id);
    });

    return index;
  };

  this.deleteProd = function (id) {
    var index = this.findIndexProd(id);
    if (index !== -1) {
      /**
       * Hàm splice giúp xóa đi phần tử trong mảng muốn xóa
       * Hàm splice có 2 tham số truyền vào
       *    - Tham số thứ 1: phải xác định dc vị trí phần tử muốn xóa
       *    - Tham số thứ 2: là số lượng phần từ muốn xóa.
       */
      this.arr.splice(index, 1);
    }
  };

  this.getProdById = function (id) {
    var prod;
    //Cach 1:
    // this.arr.forEach(function(item) {
    //   if (parseInt(item.id) === id) {
    //     empl = item;
    //   }
    // });

    prod = this.arr.find(function (item) {
      return parseInt(item.id) === parseInt(id);
    });

    return prod;
  };
};

// ProductList.prototype.updateProd = function (prod) {
//   var index = this.findIndexProd(prod.id);
//   if (index !== -1) {
//     this.arr[index] = prod;
//   }
// };
