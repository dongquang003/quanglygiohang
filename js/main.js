let productList = new ProductList();
var validation = new Validation();

const getEle = function (id) {
  return document.getElementById(id);
};

//-------------Helper function---------------
//Hàm tìm vị trí:  id => vị trí
const findById = function (id) {
  for (var i = 0; i < productList.arr.length; i++) {
    if (productList.arr[i].id === id) {
      return i;
    }
  }
  return -1;
};

//--------------Main function----------------
/**
 * Add Prod
 */
const addProd = function () {
  //1. lấy dữ liệu người dùng nhập
  const name = document.getElementById("ten").value;
  const id = document.getElementById("mssp").value;
  const image = document.getElementById("hinh").value;
  const description = document.getElementById("moTa").value;
  const price = document.getElementById("gia").value;
  const inventory = document.getElementById("soLuong").value;
  const type = document.getElementById("loai").value;
  const rating = document.getElementById("danhGia").value;

  //1.1 kiểm tra id có tồn tại trong ds chưa
  //có rồi => alert()
  //chưa => push
  // const index = findById(emplId);

  // if (index !== -1) {
  //   alert("Nhân viên đã tồn tại");
  //   return;
  // }

  var isValid = true;

  isValid &= validation.checkEmpty(name, "Ten khong dc rong!", "sp-ten");
  isValid &= validation.checkEmpty(
    description,
    "Mo ta khong dc rong!",
    "sp-moTa"
  );
  isValid &=
    validation.checkEmpty(id, "Ma khong dc rong!", "sp-ma") &&
    validation.checkLength(id, "Do dai ky tu 4-10", "sp-ma", 3, 11) &&
    validation.checkNumber(id, "Ma phai la so!", "sp-ma");
  isValid &=
    validation.checkEmpty(price, "Gia khong dc rong!", "sp-gia") &&
    validation.checkLength(price, "Do dai ky tu 6-10", "sp-gia", 3, 11) &&
    validation.checkNumber(price, "Gia phai la so!", "sp-gia");
  isValid &= validation.checkEmpty(
    inventory,
    "So luong khong duoc rong",
    "sp-soLuong"
  );
  isValid &= validation.checkPosition(
    "danhGia",
    "Phai chon loai!",
    "sp-danhGia"
  );
  isValid &= validation.checkPosition(
    "danhGia",
    "Phai chon danh gia!",
    "sp-danhGia"
  );

  if (!isValid) {
    return;
  }

  //2. tạo một đối tượng nhân viên từ dữ liệu ngta nhập
  const newProd = new Product(
    id,
    name,
    image,
    description,
    price,
    inventory,
    rating,
    type
  );

  //3. call api tới back end, nhờ thêm giùm NV mới vào DB
  axios({
    method: "POST",
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    data: newProd,
  })
    .then(function (res) {
      //call api lấy ds nhân viên mới sau khi thêm
      fetchProd();
    })
    .catch(function (err) {
      console.log(err);
    });
};

/**
 * Delete Prod
 */
function deleteProd(id) {
  // https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/employees/:id DELETE
  //  console.log('https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/employees/' + id)
  axios({
    method: "DELETE",
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" + id,
  })
    .then(function (res) {
      //call api lấy ds nhân viên mới sau khi thêm
      fetchProd();
    })
    .catch(function (err) {
      console.log(err);
    });
}

/**
 * Edit Prod
 */
function editProd(id) {
  let prod = productList.getProdById(id);
  getEle("ten").value = prod.name;
  getEle("mssp").value = prod.id;
  getEle("moTa").value = prod.description;
  getEle("gia").value = prod.price;
  getEle("hinh").value = prod.image;
  getEle("soLuong").value = prod.inventory;
  getEle("loai").value = prod.type;
  getEle("danhGia").value = prod.rating;

  getEle("btnAdd").style.display = "none";
  getEle("btnUpdate").style.display = "block";
  getEle("btnCancel").style.display = "block";
  getEle("mssp").setAttribute("disabled", true);
  /**
   * 1. viết 1 phuong thức getEmplById. Viết bên LĐT EmployeeList
   *    => return về đối tượng empl {id: "", lastName: "", firstName: "", position: "", startedDate: ""}
   * 2. Đổ dữ liệu ra ngoài các ô input.
   */
}

/**
 * Cancle
 */
function cancle() {
  getEle("btnAdd").style.display = "block";
  getEle("btnUpdate").style.display = "none";
  getEle("btnCancel").style.display = "none";

  getEle("frmProd").reset();
  getEle("mssp").removeAttribute("disabled");
}

/**
 * UPDATE
 */
function updateProd() {
  var name = getEle("ten").value;
  var id = getEle("mssp").value;
  var description = getEle("moTa").value;
  var price = getEle("gia").value;
  var image = getEle("hinh").value;
  var inventory = getEle("soLuong").value;
  var type = getEle("loai").value;
  var rating = getEle("danhGia").value;

  var prod = new Product(
    id,
    name,
    image,
    description,
    price,
    inventory,
    rating,
    type
  );

  //call api cập nhật thông tin nhân viên trong DB
  //https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/employees/:id  PUT

  axios({
    method: "PUT",
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" + id,
    //gửi đối tượng mới sau khi cập nhật lên để back end lưu lại trong db
    data: prod,
  })
    .then(function (res) {
      //call api lấy ds nhân viên mới sau khi cập nhật
      fetchProd();
    })
    .catch(function (err) {
      console.log(err);
    });
}

// Render Product
const renderProd = function (list = productList.arr) {
  var htmlContent = "";

  for (var i = 0; i < list.length; i++) {
    //template string
    htmlContent += `<tr>
      <td>${i + 1}</td>
	  <td>${list[i].name}</td>
	  <td><img src="${list[i].image}" alt="" style="width: 50px;"></td>
      <td>${list[i].id}</td>
      <td>${list[i].description}</td>
      <td>${list[i].price}</td>
      <td>${list[i].inventory}</td>
      <td>${list[i].rating}</td>
      <td>${list[i].type}</td>
      <td>
        <button class="btn btn-info" onclick="editProd(${
          list[i].id
        })">Edit</button>

        <button class="btn btn-danger" onclick="deleteProd(${
          list[i].id
        })">Delete</button>
        
        <button class="btn btn-success" onclick="goToDetail()"> View Detail</button>
      </td>
	  </tr>`;
  }
  document.getElementById("tbodyProd").innerHTML = htmlContent;
};

// Fetch Data
const fetchProd = function () {
  // hàm xử lý khi lấy dữ liệu thành công
  const resolver = function (res) {
    console.log(res.data);
    productList.arr = res.data;
    renderProd();
  };
  // hàm xử lý khi thất bại
  const rejecter = function (err) {
    console.log(err);
  };

  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    method: "GET",
  })
    //callback
    .then(resolver)
    .catch(rejecter);
};
fetchProd();

// Tìm theo ID
const fetchProdById = function () {
  // tạo input ở indext=>DOM tới lấy value => gắn id lấy được ở input vào link api
  let index;
  index = document.getElementById("txtSearch").value;

  // hàm xử lý khi lấy dữ liệu thành công
  const resolver = function (res) {
    console.log(res.data);
    // productList.arr = res.data;
    let arr = [];
    console.log(res.data);
    arr.push(res.data);
    console.log(arr);
    productList.arr = arr;

    renderProd();
  };
  // hàm xử lý khi thất bại
  const rejecter = function (err) {
    console.log(err);
  };

  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" + index,
    method: "GET",
  })
    //callback
    .then(resolver)
    .catch(rejecter);
};

getEle("txtSearch").addEventListener("keyup", () => {
  let value;
  value = getEle("txtSearch").value;

  if (value === "") {
    fetchProd();
  } else {
    fetchProdById();
  }
});
